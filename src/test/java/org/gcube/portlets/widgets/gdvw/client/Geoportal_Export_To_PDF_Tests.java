//package org.gcube.portlets.widgets.gdvw.client;
//
//import java.io.FileInputStream;
//import java.io.FileOutputStream;
//import java.io.IOException;
//import java.io.InputStream;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Properties;
//import java.util.stream.Stream;
//
//import org.gcube.application.geoportal.common.model.document.Project;
//import org.gcube.application.geoportalcommon.ConvertToDataValueObjectModel;
//import org.gcube.application.geoportalcommon.ProjectDVBuilder;
//import org.gcube.application.geoportalcommon.geoportal.GeoportalClientCaller;
//import org.gcube.application.geoportalcommon.geoportal.ProjectsCaller;
//import org.gcube.application.geoportalcommon.geoportal.UseCaseDescriptorCaller;
//import org.gcube.application.geoportalcommon.shared.geoportal.materialization.GCubeSDIViewerLayerDV;
//import org.gcube.application.geoportalcommon.shared.geoportal.materialization.innerobject.FilesetDV;
//import org.gcube.application.geoportalcommon.shared.geoportal.materialization.innerobject.PayloadDV;
//import org.gcube.application.geoportalcommon.shared.geoportal.project.ProjectDV;
//import org.gcube.application.geoportalcommon.shared.geoportal.view.ProjectView;
//import org.gcube.application.geoportalcommon.shared.geoportal.view.SectionView;
//import org.gcube.application.geoportalcommon.shared.geoportal.view.SubDocumentView;
//import org.gcube.application.geoportaldatamapper.Geoportal_JSON_Mapper;
//import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
//import org.gcube.common.scope.api.ScopeProvider;
//import org.junit.Before;
//import org.junit.Test;
//
//import com.google.gwt.core.client.GWT;
//import com.itextpdf.text.Anchor;
//import com.itextpdf.text.BaseColor;
//import com.itextpdf.text.Chunk;
//import com.itextpdf.text.Document;
//import com.itextpdf.text.Font;
//import com.itextpdf.text.FontFactory;
//import com.itextpdf.text.Paragraph;
//import com.itextpdf.text.Phrase;
//import com.itextpdf.text.pdf.PdfPCell;
//import com.itextpdf.text.pdf.PdfPTable;
//import com.itextpdf.text.pdf.PdfWriter;
//
///**
// * The Class Geoportal_Export_To_PDF_Tests.
// *
// * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
// * 
// *         Dec 5, 2022
// */
//public class Geoportal_Export_To_PDF_Tests {
//
//	private static final String GCUBE_CONFIG_PROPERTIES_FILENAME = "gcube_config.properties";
//	// APP Working Directory + /src/test/resources must be the location of
//	// gcube_config.properties
//	private static String gcube_config_path = String.format("%s/%s",
//			System.getProperty("user.dir") + "/src/test/resources", GCUBE_CONFIG_PROPERTIES_FILENAME);
//	private static String CONTEXT;
//	private static String TOKEN;
//
//	private UseCaseDescriptorCaller clientUCD;
//	private ProjectsCaller clientProjects;
//
//	// private static String PROFILE_ID = "concessioni-estere";
//	private static String PROFILE_ID = "profiledConcessioni";
//	private static String PROJECT_ID = "6384d44d308f5c28c5ee0a01"; // 63d011c4dcac4551b9a6b930
//																	// //6442653ac6a0422d9e9042e0
//																	// //6399de3ca0a4545420373251
//
//	// IMPORTANT!!!! #NB SET USERNAME = null to test PUBLIC ACCESS
//
//	private static String USERNAME = "francesco.mangiacrapa";
//
//	/**
//	 * Read context settings.
//	 */
//	public static void readContextSettings() {
//
//		try (InputStream input = new FileInputStream(gcube_config_path)) {
//
//			Properties prop = new Properties();
//
//			// load a properties file
//			prop.load(input);
//
//			CONTEXT = prop.getProperty("CONTEXT");
//			TOKEN = prop.getProperty("TOKEN");
//			// get the property value and print it out
//			System.out.println("CONTEXT: " + CONTEXT);
//			System.out.println("TOKEN: " + TOKEN);
//
//		} catch (IOException ex) {
//			ex.printStackTrace();
//		}
//	}
//
//	/**
//	 * Gets the client.
//	 *
//	 * @return the client
//	 */
//	@Before
//	public void getClient() {
//		readContextSettings();
//		// assumeTrue(GCubeTest.isTestInfrastructureEnabled());
//		ScopeProvider.instance.set(CONTEXT);
//		SecurityTokenProvider.instance.set(TOKEN);
//		clientUCD = GeoportalClientCaller.useCaseDescriptors();
//		clientProjects = GeoportalClientCaller.projects();
//	}
//
//	/**
//	 * Test read project view.
//	 */
//	@Test
//	public void testExportToPDF() {
//
//		try {
//			ScopeProvider.instance.set(CONTEXT);
//			SecurityTokenProvider.instance.set(TOKEN);
//			Project theProject = clientProjects.getProjectByID(PROFILE_ID, PROJECT_ID);
//			ProjectDVBuilder projectBuilder = ProjectDVBuilder.newBuilder().fullDocumentMap(true);
//			projectBuilder.relationships(true);
//
//			System.out.println("project json: " + theProject.getTheDocument().toJson());
//
//			ProjectDV theProjectDV = ConvertToDataValueObjectModel.toProjectDV(theProject, projectBuilder);
//			ProjectView projectView = Geoportal_JSON_Mapper.loadProjectView(theProjectDV, CONTEXT, USERNAME);
//			Geoportal_JSON_Mapper.prettyPrintProjectView(projectView);
//
//			//CREATING NEW PDF
//			Document document = new Document();
//			PdfWriter.getInstance(document, new FileOutputStream("iTextHelloWorld.pdf"));
//			document.open();
//			
//			Font font = FontFactory.getFont(FontFactory.COURIER, 16, BaseColor.BLACK);
//			Chunk chunk = new Chunk("Hello World", font);
//
//			document.add(chunk);
//			
//			for (SectionView section : projectView.getListSections()) {
//
//				List<SubDocumentView> subdocs = section.getListSubDocuments();
//				section.getSectionTitle();
//
//				if (subdocs != null && subdocs.size() > 0) {
//					document = appendSubDocumentToPDF(document, section, subdocs);
//				}
//
//			}
//			
//			document.close();
//
////			ProjectView projectView = Geoportal_JSON_Mapper.loadProjectView(theProjectDV, CONTEXT, USERNAME);
////			Geoportal_JSON_Mapper.prettyPrintProjectView(projectView);
//			System.out.println("\n\n testReadProjectView terminated!!!");
//		} catch (Exception e) {
//			System.err.println("Error");
//			e.printStackTrace();
//		}
//	}
//
//	private Document appendSubDocumentToPDF(Document document, SectionView sectionView,
//			List<SubDocumentView> subDocuments) {
//		try {
//			boolean displayAsGallery = false;
//
//			for (SubDocumentView subDocumentView : subDocuments) {
//				if (subDocumentView.getListImages() != null && subDocumentView.getListImages().size() > 0) {
//					GWT.log("Section with images: " + subDocumentView.getListImages());
//					displayAsGallery = true;
//					break;
//				}
//			}
//
//			boolean displayAsMapOfLayers = false;
//			for (SubDocumentView subDocumentView : subDocuments) {
//				if (subDocumentView.getListLayers() != null && subDocumentView.getListLayers().size() > 0) {
//					GWT.log("Section with layers: " + subDocumentView.getListImages());
//					displayAsMapOfLayers = true;
//					break;
//				}
//			}
//
//			// Displaying the whole section as a Gallery
//			if (displayAsGallery) {
//				GWT.log("displayAsGallery the: " + sectionView);
//
//				// Displaying the whole section as a Map of Layers
//			} else if (displayAsMapOfLayers) {
//				GWT.log("displayAsMapOfLayers the: " + sectionView);
//				for (SubDocumentView subDocumentView : subDocuments) {
////				String table = GeoportalDataViewerConstants.jsonToTableHTML(subDocumentView.getMetadataAsJSON());
////				sectionPanelContainer.add(new HTML(table));
//
//					List<GCubeSDIViewerLayerDV> layers = subDocumentView.getListLayers();
//					if (layers != null) {
//						for (GCubeSDIViewerLayerDV gCubeLayer : layers) {
////							LayersSectionViewer layerSectionViewer = new LayersSectionViewer(gCubeLayer,
////									subDocumentView);
////							sectionPanelContainer.add(layerSectionViewer);
////							// showLinkToDownloadWsContent(fileset.getName(), fileset.getListPayload());
//						}
//					}
//				}
//
//			} else {
//				GWT.log("displaying default the: " + sectionView);
//				for (SubDocumentView subDocumentView : subDocuments) {
//					PdfPTable table = jsonToPDFTable(subDocumentView.getMetadataAsJSON());
//					document.add(table);
//
//					List<FilesetDV> files = subDocumentView.getListFiles();
//					if (files != null) {
//						for (FilesetDV fileset : files) {
//					        Paragraph paragraph = new Paragraph(fileset.getGcubeProfileFieldName());
//					        document.add(paragraph);
//							List<Anchor> anchors = toPDFAnchors(fileset.getListPayload());
//							for (Anchor anchor : anchors) {
//								document.add(anchor);
//							}
//						}
//					}
//				}
//			}
//		} catch (Exception e) {
//			GWT.log("Error on rendering the section: " + e.getMessage());
//		}
//		
//		return document;
//
//	}
//
//	private static List<Anchor> toPDFAnchors(List<PayloadDV> payloads) {
//
//		List<Anchor> anchors = new ArrayList<Anchor>(payloads.size());
//		for (PayloadDV payloadDV : payloads) {
//			Anchor anchor = new Anchor(payloadDV.getName());
//			anchor.setReference(payloadDV.getLink());
//			anchors.add(anchor);
//		}
//
//		return anchors;
//
//	}
//	
////	toPDFImage(){
////		
////	}
//
//	private static PdfPTable jsonToPDFTable(String json) {
//		PdfPTable table = new PdfPTable(3);
//		addTableHeader(table);
//		addRows(table);
//		// addCustomRows(table);
//		return table;
//	}
//
//	private static void addTableHeader(PdfPTable table) {
//		Stream.of("column header 1", "column header 2", "column header 3").forEach(columnTitle -> {
//			PdfPCell header = new PdfPCell();
//			header.setBackgroundColor(BaseColor.LIGHT_GRAY);
//			header.setBorderWidth(2);
//			header.setPhrase(new Phrase(columnTitle));
//			table.addCell(header);
//		});
//	}
//
//	private static void addRows(PdfPTable table) {
//		table.addCell("row 1, col 1");
//		table.addCell("row 1, col 2");
//		table.addCell("row 1, col 3");
//	}
//
//}
