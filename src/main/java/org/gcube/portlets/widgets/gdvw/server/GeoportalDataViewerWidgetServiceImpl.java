package org.gcube.portlets.widgets.gdvw.server;

import org.gcube.application.geoportalcommon.shared.geoportal.project.ProjectDV;
import org.gcube.application.geoportalcommon.shared.geoportal.view.ProjectView;
import org.gcube.application.geoportaldatamapper.Geoportal_JSON_Mapper;
import org.gcube.portlets.widgets.gdvw.client.GeoportalDataViewerWidgetService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;

/**
 * The server-side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class GeoportalDataViewerWidgetServiceImpl extends RemoteServiceServlet implements GeoportalDataViewerWidgetService {

	private static final Logger LOG = LoggerFactory.getLogger(GeoportalDataViewerWidgetServiceImpl.class);

	/**
	 * Gets the project view for id.
	 *
	 * @param profileID the profile ID
	 * @param projectID the project ID
	 * @return the project view for id
	 * @throws Exception the exception
	 */
	@Override
	public ProjectView getProjectView(ProjectDV theProjectDV) throws Exception {
		LOG.info("called getProjectView");

		if (theProjectDV == null)
			throw new Exception("Bad Parameter: input project is null");
		try {
			LOG.info("getProjectView for theProjectDV: {}, ProfileID:  {}", theProjectDV.getId(),
					theProjectDV.getProfileID());
			String scope = SessionUtil.getCurrentContext(this.getThreadLocalRequest(), true);
			String userName = null;
			try {
				userName = SessionUtil.getCurrentUser(this.getThreadLocalRequest()).getUsername();
			} catch (Exception e) {
				LOG.info("User not found in session, the userName for checking the policy will  be null");
			}

			ProjectView projectView = Geoportal_JSON_Mapper.loadProjectView(theProjectDV, scope, userName);

			if (LOG.isTraceEnabled()) {
				Geoportal_JSON_Mapper.prettyPrintProjectView(projectView);
			}

			LOG.info("returning project view for id: " + projectView.getTheProjectDV().getId());
			return projectView;

		} catch (Exception e) {
			String erroMsg = "Error occurred on creating projectView for id: " + theProjectDV.getId();
			LOG.error(erroMsg, e);
			throw new Exception(erroMsg);
		}

	}
	
}
