package org.gcube.portlets.widgets.gdvw.client;

import org.gcube.application.geoportalcommon.ConvertToDataViewModel;

import com.google.gwt.dom.client.Element;
import com.google.gwt.i18n.client.DateTimeFormat;

/**
 * The Class GeoportalDataViewerWidgetConstants.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Apr 27, 2023
 */
public class GeoportalDataViewerWidgetConstants {

	public static final DateTimeFormat DATE_TIME_FORMAT = DateTimeFormat.getFormat("dd MMMM yyyy");

	public static DateTimeFormat DT_FORMAT = DateTimeFormat.getFormat(ConvertToDataViewModel.DATE_FORMAT);

	/**
	 * Prints the.
	 *
	 * @param msg the msg
	 */
	public static native void printJs(String msg)/*-{
		//console.log("js console: " + msg);
	}-*/;

	/**
	 * Prints the.
	 *
	 * @param object the object
	 */
	public static native void printJsObj(Object object)/*-{
		//console.log("js obj: " + JSON.stringify(object, null, 4));
	}-*/;

	/**
	 * Prints the.
	 *
	 * @param object the object
	 * @return the string
	 */
	public static native String toJsonObj(Object object)/*-{
		return JSON.stringify(object);
	}-*/;

	/**
	 * Json to HTML.
	 *
	 * @param jsonTxt the json txt
	 * @return the string
	 */
	public static native String jsonToTableHTML(String jsonTxt)/*-{
		try {
			var jsonObj = JSON.parse(jsonTxt);

			if (jsonObj.length == undefined)
				jsonObj = [ jsonObj ]
				//console.log(jsonObj.length)

				// EXTRACT VALUE FOR HTML HEADER.
			var col = [];
			for (var i = 0; i < jsonObj.length; i++) {
				for ( var key in jsonObj[i]) {
					//console.log('key json' +key)
					if (col.indexOf(key) === -1) {
						col.push(key);
					}
				}
			}

			// CREATE DYNAMIC TABLE.
			var table = document.createElement("table");
			try {
				table.classList.add("w-my-html-table");

			} catch (e) {
				console.log('invalid css add', e);
			}

			// ADD JSON DATA TO THE TABLE AS ROWS.
			for (var i = 0; i < col.length; i++) {
				tr = table.insertRow(-1);
				var firstCell = tr.insertCell(-1);
				//firstCell.style.cssText="font-weight: bold; text-align: center; vertical-align: middle;";
				firstCell.innerHTML = col[i];
				for (var j = 0; j < jsonObj.length; j++) {
					var tabCell = tr.insertCell(-1);
					var theValue = jsonObj[j][col[i]];
					//console.log("the value: "+theValue);
					if (theValue !== null
							&& Object.prototype.toString.call(theValue) === '[object Array]') {
						var formattedValueArray = "";
						for (var k = 0; k < theValue.length; k++) {
							var theValueArray = theValue[k];
							//console.log(theValueArray);
							formattedValueArray += theValueArray + "<br>";
						}
						tabCell.innerHTML = formattedValueArray;
					} else {
						tabCell.innerHTML = theValue;
					}
				}
			}

			return table.outerHTML;
		} catch (e) {
			console.log('invalid json', e);
			return jsonTxt;
		}

	}-*/;

	/**
	 * Click element.
	 *
	 * @param elem the elem
	 */
	public static native void clickElement(Element elem) /*-{
		elem.click();
	}-*/;

}
