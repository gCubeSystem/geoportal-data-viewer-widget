package org.gcube.portlets.widgets.gdvw.client;

import org.gcube.application.geoportalcommon.shared.geoportal.project.ProjectDV;
import org.gcube.application.geoportalcommon.shared.geoportal.view.ProjectView;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * The async counterpart of <code>GeoportalDataViewerWidgetService</code>.
 */
public interface GeoportalDataViewerWidgetServiceAsync {

	void getProjectView(ProjectDV theProjectDV, AsyncCallback<ProjectView> callback);
}
