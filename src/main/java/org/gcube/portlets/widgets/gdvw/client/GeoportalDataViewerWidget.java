package org.gcube.portlets.widgets.gdvw.client;

import org.gcube.application.geoportalcommon.shared.geoportal.view.ProjectView;
import org.gcube.portlets.widgets.gdvw.client.project.ProjectViewer;

import com.google.gwt.core.client.GWT;
import com.google.gwt.core.client.ScriptInjector;

/**
 * The Class GeoportalDataViewerWidget.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Apr 27, 2023
 */
public class GeoportalDataViewerWidget {
	/**
	 * The message displayed to the user when the server cannot be reached or
	 * returns an error.
	 */
	private static final String SERVER_ERROR = "An error occurred while "
			+ "attempting to contact the server. Please check your network " + "connection and try again.";

	private final GeoportalDataViewerWidgetServiceAsync geoportalDataViewerWidgetService = GWT
			.create(GeoportalDataViewerWidgetService.class);

	/**
	 * This is the entry point method.
	 */
	public GeoportalDataViewerWidget() {

		ScriptInjector.fromUrl("//cdnjs.cloudflare.com/ajax/libs/nanogallery2/3.0.5/jquery.nanogallery2.min.js")
				.setWindow(ScriptInjector.TOP_WINDOW).inject();

	}

	/**
	 * Gets the project viewer.
	 *
	 * @param projectView the project view
	 * @return the project viewer
	 */
	public ProjectViewer getProjectViewer(ProjectView projectView) {

		return new ProjectViewer(projectView);
	}
}
