/**
 *
 */
package org.gcube.portlets.widgets.gdvw.server;

import javax.servlet.http.HttpServletRequest;

import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.portal.PortalContext;
import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.vomanagement.usermanagement.GroupManager;
import org.gcube.vomanagement.usermanagement.exception.GroupRetrievalFault;
import org.gcube.vomanagement.usermanagement.exception.UserManagementSystemException;
import org.gcube.vomanagement.usermanagement.impl.LiferayGroupManager;
import org.gcube.vomanagement.usermanagement.model.GCubeGroup;
import org.gcube.vomanagement.usermanagement.model.GCubeUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class SessionUtil.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Apr 27, 2023
 */
public class SessionUtil {

	/** The log. */
	private static Logger LOG = LoggerFactory.getLogger(SessionUtil.class);

	/**
	 * Checks if is session expired.
	 *
	 * @param httpServletRequest the http servlet request
	 * @return true, if is session expired
	 * @throws Exception the exception
	 */
	public static boolean isSessionExpired(HttpServletRequest httpServletRequest) throws Exception {
		LOG.trace("workspace session validating...");
		return PortalContext.getConfiguration().getCurrentUser(httpServletRequest) == null;
	}

	/**
	 * Retrieve the current user by using the portal manager.
	 *
	 * @param request the request
	 * @return a GcubeUser object
	 */
	public static GCubeUser getCurrentUser(HttpServletRequest request) {

		if (request == null)
			throw new IllegalArgumentException("HttpServletRequest is null!");

		PortalContext pContext = PortalContext.getConfiguration();
		GCubeUser user = pContext.getCurrentUser(request);
		LOG.debug("Returning user " + user);
		return user;
	}

	/**
	 * Retrieve the current scope by using the portal manager.
	 *
	 * @param request     the request
	 * @param setInThread the set in thread
	 * @return a GcubeUser object
	 */
	public static String getCurrentContext(HttpServletRequest request, boolean setInThread) {

		if (request == null)
			throw new IllegalArgumentException("HttpServletRequest is null!");

		PortalContext pContext = PortalContext.getConfiguration();
		String context = pContext.getCurrentScope(request);

		if (context != null && setInThread)
			ScopeProvider.instance.set(context);

		LOG.debug("Returning context " + context);
		return context;
	}

	/**
	 * Retrieve the current scope by using the portal manager.
	 *
	 * @param request     the request
	 * @param setInThread the set in thread
	 * @return a GcubeUser object
	 */
	public static String getCurrentToken(HttpServletRequest request, boolean setInThread) {

		if (request == null)
			throw new IllegalArgumentException("HttpServletRequest is null!");

		PortalContext pContext = PortalContext.getConfiguration();
		String scope = pContext.getCurrentScope(request);
		GCubeUser user = pContext.getCurrentUser(request);
		String token = PortalContext.getConfiguration().getCurrentUserToken(scope, user.getUsername());

		if (token != null) {
			LOG.debug("Returning token " + token.substring(1, 10) + "_MASKED_TOKEN_");
			if (setInThread)
				SecurityTokenProvider.instance.set(token);
		}

		return token;
	}

	/**
	 * Retrieve the group given the scope.
	 *
	 * @param scope the scope
	 * @return the group from scope
	 * @throws UserManagementSystemException the user management system exception
	 * @throws GroupRetrievalFault           the group retrieval fault
	 */
	public static GCubeGroup getGroupFromScope(String scope) throws UserManagementSystemException, GroupRetrievalFault {

		if (scope == null || scope.isEmpty())
			throw new IllegalArgumentException("Scope is missing here!!");

		GroupManager gm = new LiferayGroupManager();
		long groupId = gm.getGroupIdFromInfrastructureScope(scope);
		return gm.getGroup(groupId);

	}

}
