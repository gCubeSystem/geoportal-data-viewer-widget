package org.gcube.portlets.widgets.gdvw.client.project;

import java.util.Map.Entry;

import org.gcube.application.geoportalcommon.shared.GeoportalItemReferences;
import org.gcube.application.geoportalcommon.shared.geoportal.DocumentDV;
import org.gcube.application.geoportalcommon.shared.geoportal.view.ProjectView;
import org.gcube.application.geoportalcommon.shared.geoportal.view.SectionView;

import com.github.gwtbootstrap.client.ui.Button;
import com.github.gwtbootstrap.client.ui.constants.ButtonType;
import com.github.gwtbootstrap.client.ui.constants.IconType;
import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.UListElement;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.uibinder.client.UiBinder;
import com.google.gwt.uibinder.client.UiField;
import com.google.gwt.user.client.Random;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HTMLPanel;
import com.google.gwt.user.client.ui.Widget;

/**
 * The Class ProjectViewer.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Apr 27, 2023
 */
public class ProjectViewer extends Composite {

	private static ProjectViewerUiBinder uiBinder = GWT.create(ProjectViewerUiBinder.class);

	/**
	 * The Interface ProjectViewerUiBinder.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
	 * 
	 *         Nov 9, 2022
	 */
	interface ProjectViewerUiBinder extends UiBinder<Widget, ProjectViewer> {
	}

	@UiField
	HTMLPanel headerPanel;

	@UiField
	HTMLPanel pageViewDetails;

	@UiField
	HTMLPanel projectViewerMainPanel;

	@UiField
	HTMLPanel centroidPanel;

	@UiField
	HTMLPanel tableOfContentPanel;

	@UiField
	HTMLPanel toc_container;

	@UiField
	HTMLPanel toc_list_container;

	@UiField
	Button reduceToc;

	@UiField
	UListElement toc_list_anchors;

	private ProjectView theProjectView;

	private GeoportalItemReferences geoportalItemReferences;

	private String projectViewerMainPanelID;

	/**
	 * Instantiates a new project viewer.
	 */
	private ProjectViewer() {
		initWidget(uiBinder.createAndBindUi(this));
		pageViewDetails.getElement().addClassName("w-page-view-details");
		projectViewerMainPanelID = "projectViewer-" + Random.nextInt();
		projectViewerMainPanel.getElement().setId(projectViewerMainPanelID);
	}

	/**
	 * Instantiates a new project viewer.
	 *
	 * @param applicationBus the application bus
	 * @param projectView    the project view
	 */
	public ProjectViewer(final ProjectView projectView) {
		this();
		GWT.log("Rendering " + projectView.getTheProjectDV().getId());
		this.theProjectView = projectView;

		DocumentDV document = projectView.getTheProjectDV().getTheDocument();
		if (document != null) {
			Entry<String, Object> firstEntrySet = document.getFirstEntryOfMap();
			if (firstEntrySet != null) {
				final String theTitle = firstEntrySet.getKey() + ": " + firstEntrySet.getValue();
				headerPanel.add(new HTML(theTitle));
			}
		}

		reduceToc.setType(ButtonType.LINK);
		reduceToc.setIcon(IconType.PLUS_SIGN_ALT);
		toc_list_container.setVisible(false);
		reduceToc.addClickHandler(new ClickHandler() {

			@Override
			public void onClick(ClickEvent event) {
				boolean visible = toc_list_container.isVisible();
				setTocContentVisible(!visible);

//				if (visible) {
//					toc_list_container.setVisible(false);
//					reduceToc.setIcon(IconType.PLUS_SIGN_ALT);
//				} else {
//					toc_list_container.setVisible(true);
//					reduceToc.setIcon(IconType.MINUS_SIGN_ALT);
//				}
			}
		});

		for (SectionView sectionView : projectView.getListSections()) {

			if (!sectionView.isEmpty()) {
				SectionViewer sectionViewer = new SectionViewer(sectionView, projectViewerMainPanelID);
				String sectionId = sectionView.getSectionTitle().replaceAll("[^A-Za-z0-9]", "-") + "_"
						+ Random.nextInt();
				String divTarget = "<div class='anchor-target' id='" + sectionId + "'></div>";
				sectionViewer.getElement().insertFirst(new HTML(divTarget).getElement());
				addAnchorToSection(sectionId, sectionView.getSectionTitle());
				pageViewDetails.add(sectionViewer);
			}

		}

		if (toc_list_anchors.getChildCount() > 0) {
			tableOfContentPanel.setVisible(true);
		}
	}

	/**
	 * Adds the anchor to section.
	 *
	 * @param id   the id
	 * @param text the text
	 */
	private void addAnchorToSection(String id, String text) {
		String htmlAnchor = "<a href='#" + id + "'>" + text + "</a>";
		toc_list_anchors.appendChild(new HTML("<li>" + htmlAnchor + "</li>").getElement());
	}

	/**
	 * Sets the toc content visible.
	 *
	 * @param bool the new toc content visible
	 */
	public void setTocContentVisible(boolean bool) {

		toc_list_container.setVisible(bool);

		if (bool) {
			reduceToc.setIcon(IconType.MINUS_SIGN_ALT);
		} else {
			reduceToc.setIcon(IconType.PLUS_SIGN_ALT);
		}

	}

	/**
	 * Gets the project view.
	 *
	 * @return the project view
	 */
	public ProjectView getProjectView() {
		return theProjectView;
	}

}
