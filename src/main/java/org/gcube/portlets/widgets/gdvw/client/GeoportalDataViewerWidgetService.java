package org.gcube.portlets.widgets.gdvw.client;

import org.gcube.application.geoportalcommon.shared.geoportal.project.ProjectDV;
import org.gcube.application.geoportalcommon.shared.geoportal.view.ProjectView;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * The client-side stub for the RPC service.
 */
@RemoteServiceRelativePath("greet")
public interface GeoportalDataViewerWidgetService extends RemoteService {

	/**
	 * Gets the project view for id.
	 *
	 * @param profileID the profile ID
	 * @param projectID the project ID
	 * @return the project view for id
	 * @throws Exception the exception
	 */
	ProjectView getProjectView(ProjectDV theProjectDV) throws Exception;
}
