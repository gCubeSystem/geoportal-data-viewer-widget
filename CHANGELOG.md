
# Changelog

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v1.0.1]

- Updated `geoportal-data-mapper` range at [1.0.1, 2.0.0-SNAPSHOT)

## [v1.0.0]

#### First release

- Created and integrated the widget [#25015]